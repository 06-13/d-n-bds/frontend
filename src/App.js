import React, { Component, useEffect, useState } from "react";
import "antd/dist/antd.css";
import "./index.css";
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  TeamOutlined,
  ClusterOutlined,
  BookOutlined,
  GlobalOutlined,
  GoldOutlined,
  BankOutlined,
  AuditOutlined,
  DollarCircleOutlined,
  ContainerOutlined,
  FolderOutlined,
  LikeOutlined,
  ImportOutlined,
} from "@ant-design/icons";
import { Button, Col, Layout, Menu } from "antd";
import { BrowserRouter, Link, NavLink, Router } from "react-router-dom";
import MainPage from "./components/router";
import Login from "./components/login/login";

  const { Header, Sider, Content } = Layout;

const Table = () => {

  const [collapsed, setCollapsed] = useState(false);
  const [current, setCurrent] = useState("");
  const onClick = (e) => {
    setCurrent(e.key);
  };



const [token,setToken] = useState("");
      if(!token){
        return (
          <div>
          <Col span={12}  className="login">
          <Login callBackSetToken = {(token) => setToken(token)}></Login>
          </Col>
          </div>
        )
      }else {
      return (
        <BrowserRouter>
          <Layout>
            <Sider trigger={null} collapsible collapsed={collapsed}>
              <div className="logo"></div>
              <Menu
                theme="dark"
                mode="inline"
                onClick={onClick}
                selectedKeys={[current]}
                defaultSelectedKeys={["employee"]}
                items={[
                  {
                    key: "address",
                    icon: <GlobalOutlined />,
                    label: <NavLink to="/">Địa chỉ toạ độ</NavLink>,
                  },
                  {
                    key: "masterlayout",
                    icon: <GoldOutlined />,
                    label: <NavLink to="/masterlayout">Mặt bằng tầng</NavLink>,
                  },
                  {
                    key: "contractor",
                    icon: <BankOutlined />,
                    label:<NavLink to="/contractor">Nhà thầu xây dựng</NavLink>,
                  },
                  {
                    key: "designunit",
                    icon: <AuditOutlined />,
                    label:<NavLink to="/designunit">Đơn vị thiết kế</NavLink>,
                  },
                  {
                    key: "investor",
                    icon: <DollarCircleOutlined />,
                    label: <NavLink to="/investor">Nhà đầu tư</NavLink>,
                  },
                  {
                    key: "regionlink",
                    icon: <ClusterOutlined />,
                    label: <NavLink to="/regionlink">Kết nối vùng</NavLink>,
                  },
                  {
                    key: "utilities",
                    icon: <BookOutlined />,
                    label: <NavLink to="/utilities">Tiện ích</NavLink>,
                  },
                  {
                    key: "employee",
                    icon: <TeamOutlined />,
                    label: <NavLink to="/employee">Khách hàng</NavLink>,
                  },
                  {
                    key: "customer",
                    icon: <UserOutlined />,
                    label: <NavLink to="/customer">Người dùng</NavLink>,
                  },
                  {
                    key: "10",
                    icon: <ContainerOutlined />,
                    label: <NavLink to="/project">Dự án xây dựng</NavLink>,
                  },
                  {
                    key: "11",
                    icon: <FolderOutlined />,
                    label: "Thông tin BĐS",
                  },
                  {
                    key: "subscriptions",
                    icon: <LikeOutlined />,
                    label: <NavLink to="/subscriptions">Subscriptions</NavLink>,
                  },
                  {
                    key: "Logout",
                    icon: <ImportOutlined />,
                    label:<label>Logout</label>,
                    onClick:() => setToken("")
                  },
                ]}
              />
            </Sider>
            <Layout className="site-layout">
              <Header
                className="site-layout-background"
                style={{
                  padding: 0,
                }}
              >
                {React.createElement(
                  collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                  {
                    className: "trigger",
                    onClick: () => setCollapsed(!collapsed),
                  }
                )}
              </Header>
              <Content
                className="site-layout-background"
                style={{
                  margin: "1.5rem 1rem",
                  padding: 24,
                  minHeight: 280,
                }}
              >
                <MainPage></MainPage>
              </Content>
            </Layout>
          </Layout>
        </BrowserRouter>
      );
              }
};

export default Table;
