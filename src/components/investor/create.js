import React, { useState } from "react";
import Modal from "antd/lib/modal/Modal";
import { Input } from "antd";

export default function Create({ trangThai, callbackToggle }) {
  const [data, setData] = useState({
    name: "",
    description: "",
    projects: "",
    address:"",
    phone:"",
    phone2:"",
    fax:"",
    email:"",
    website:"",
    note:"",
  });
  return (
    <Modal
      title="Tạo mới nhà đầu tư"
      centered
      visible={trangThai}
      onOk={() => {
        console.log(data);
        if (!data.name || !data.description || !data.projects || !data.address || !data.phone || !data.phone2 || !data.fax || !data.email || !data.website || !data.note) {
          alert("Cần nhập đầy đủ dữ liệu");
        } else {
          let response = fetch("http://localhost:8080/BDS/investor", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
          })
            .then((data) => data.json())
            .then((data) => {
              alert("Tạo mới nhà đầu tư thành công")
              callbackToggle(false);
            });
        }
      }}
      onCancel={() => callbackToggle(false)}
      width={1000}
    >
      <h3>Tên</h3>
      <Input
        placeholder="Tên ..."
        onChange={(event) => (data.name = event.target.value)}
      ></Input>
      <h3>Mô tả</h3>
      <Input
        placeholder="Mô tả ..."
        onChange={(event) => (data.description = event.target.value)}
      ></Input>
      <h3>Dự án</h3>
      <Input
        placeholder="Dự án ..."
        onChange={(event) => (data.projects = event.target.value)}
      ></Input>
      <h3>Địa chỉ</h3>
      <Input
        placeholder="Dự án ..."
        onChange={(event) => (data.address = event.target.value)}
      ></Input>
      <h3>Số điện thoại</h3>
      <Input
        placeholder="Số điện thoại ..."
        onChange={(event) => (data.phone = event.target.value)}
      ></Input>
      <h3>Số điện thoại khác</h3>
      <Input
        placeholder="Số điện thoại khác ..."
        onChange={(event) => (data.phone2 = event.target.value)}
      ></Input>
      <h3>Fax</h3>
      <Input
        placeholder="Fax ..."
        onChange={(event) => (data.fax = event.target.value)}
      ></Input>
      <h3>Email</h3>
      <Input
        placeholder="Email ..."
        onChange={(event) => (data.email = event.target.value)}
      ></Input>
      <h3>Trang chủ</h3>
      <Input
        placeholder="Trang chủ ..."
        onChange={(event) => (data.website = event.target.value)}
      ></Input>
      <h3>Ghi chú</h3>
      <Input
        placeholder="Ghi chú ..."
        onChange={(event) => (data.note = event.target.value)}
      ></Input>
    </Modal>
  );
}
