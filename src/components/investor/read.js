import React, { useEffect, useState } from "react";
import Modal from "antd/lib/modal/Modal";
import { Input } from "antd";

export default function Read({ trangThai, callbackToggle, dataRead }) {
  return (
    <Modal
      title="Thông tin nhà đầu tư"
      centered
      visible={trangThai}
      onOk={() => callbackToggle(false)}
      onCancel={() => callbackToggle(false)}
      width={1000}
    >
      <h3>ID</h3>
      <Input value={dataRead.id}></Input>
      <h3>Tên</h3>
      <Input value={dataRead.name}></Input>
      <h3>Mô tả</h3>
      <Input value={dataRead.description}></Input>
      <h3>Dự án</h3>
      <Input value={dataRead.projects}></Input>
      <h3>Địa chỉ</h3>
      <Input value={dataRead.address}></Input>
      <h3>Số điện thoại</h3>
      <Input value={dataRead.phone}></Input>
      <h3>Số điện thoại khác</h3>
      <Input value={dataRead.phone2}></Input>
      <h3>Fax</h3>
      <Input value={dataRead.fax}></Input>
      <h3>Email</h3>
      <Input value={dataRead.email}></Input>
      <h3>Trang chủ</h3>
      <Input value={dataRead.website}></Input>
      <h3>Ghi chú</h3>
      <Input value={dataRead.note}></Input>
    </Modal>
  );
}
