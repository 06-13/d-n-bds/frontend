import React , { useEffect, useState } from 'react';
import Modal from 'antd/lib/modal/Modal';
import { Input } from 'antd';
export default function ModalRead({trangThai,callbackToggle,readData,name}) {
    const [data,setData] = useState([]);
    useEffect(() => {
        setData(Object.entries(readData));
    },[readData]);
  return (
    <Modal
        title={name}
        centered
        visible={trangThai}
        onOk={() => {
            
        }}
        onCancel={() => callbackToggle(false)}
        width={1000}
      >
        {data.map(data => { return (
            <>
            <label>{data[0]}</label>
            <Input value={data[1]}></Input>
            </>
        )
      })}
      </Modal>
  )
}
