import React, { useEffect, useState } from "react";
import { Select , Button } from "antd";

export default function Ward() {
  const [provinceData, setProvinceData] = useState([]);
  const [districtData, setDistrictData] = useState([]);
  const [wardData, setWardData] = useState([]);

  useEffect(() => {
    loadProvinceData();
  },[]);
  const loadProvinceData = () => {
    let response = fetch("http://localhost:8900/provinces", {
      method: "GET",
    })
      .then((data) => data.json())
      .then((data) => {
        setProvinceData(data);
        console.log(data);
      });
  };
  const handleChangeProvince = (value) => {
        if(value === "default"){
            return;
        }
        let response = fetch(
            "http://localhost:8900/provinces/" + value + "/districts",
            {
              method: "GET",
            }
          )
            .then((data) => data.json())
            .then((data) => {
              setDistrictData(data);
            });
  };

  const handleChangeDistrict = (value) => {
    if(value === "default"){
        return;
    }
    let response = fetch(
      "http://localhost:8900/districts/" + value + "/wards",
      {
        method: "GET",
      }
    )
      .then((data) => data.json())
      .then((data) => {
        setWardData(data);
      });
  };

  const handleChangeWard = (value) => {

  }

  return (
    <>
      <div>
        <Select
          defaultValue={"default"}
          style={{
            width: "40%",
          }}
          onChange={handleChangeProvince}
        >
          <option value={"default"}>Thông tin tỉnh / thành phố</option>
          {provinceData.map((data) => {
            return <option value={data.province_id}>{data.name}</option>;
          })}
        </Select>
      </div>

      <div>
        <Select
          defaultValue={"default"}
          style={{
            width: "40%",
          }}
          onChange={handleChangeDistrict}
        >
          <option value={"default"}>Thông tin quận / huyện </option>
          {districtData.map((data) => {
            return (
                <option value={data.districtId}>{data.name}</option>
            )
          })}
        </Select>
      </div>

      <div>
        <Select
          style={{
            width: "40%",
          }}
          defaultValue={"default"}
          onChange={handleChangeWard}
        >
          <option value={"default"}>Thông tin xã / phường </option>
          {wardData.map((data) => {
            return <option value={data.districtId}>{data.name}</option>;
          })}
        </Select>
      </div>
      <div>
          <Button>Submit</Button>
      </div>
    </>
  );
}
