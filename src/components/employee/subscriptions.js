import { Table } from "antd";
import React, { useState, useEffect } from "react";
import TableForm from "../table/table";
import "./index.css";
import { Popconfirm, Button } from "antd";
import "antd/dist/antd.css";
import {
  SearchOutlined,
  DeleteOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { PlusOutlined } from "@ant-design/icons";
export default function Subscriptions() {
  const [data, setData] = useState();
  async function getData(url = "http://localhost:8080/BDS/subscriptions") {
    const response = await fetch(url, {
      body: JSON.stringify(data),
    });
    return response.json();
  }
  useEffect(() => {
    getData().then((data) => {
      setData(data);
    });
  }, []);
  const dataSource = data;
  const [visible, setVisible] = useState(false);
  const defaultColumns = [
    {
      render: (_, record) =>
        dataSource.length >= 1 ? (
          <Popconfirm title="Sure to view?">
            <SearchOutlined />
          </Popconfirm>
        ) : null,
        width:"1%",
    },
    {
      render: (_, record) =>
        dataSource.length >= 1 ? (
          <Popconfirm title="Sure to edit?">
            <EditOutlined />
          </Popconfirm>
        ) : null,
      width:"1%",
    },
    {
      render: (_, record) =>
        dataSource.length >= 1 ? (
          <Popconfirm title="Sure to delete?">
            <DeleteOutlined />
          </Popconfirm>
        ) : null,
      width:"1%",
    },
    {
      title: "ID",
      dataIndex: "id",
      width:"3%",
    },
    {
      title: "User",
      dataIndex: "user",
      width:"4%",
    },
    {
      title: "Endpoint",
      dataIndex: "endpoint",
    },
    {
      title: "Publickey",
      dataIndex: "publickey",
      width:"20%",
    },
    {
      title: "Authentication Token",
      dataIndex: "authenticationtoken",
      width:"10%",
    },
    {
      title: "Content Encoding",
      dataIndex: "contentencoding",
      width:"10%",
    },
  ];

  return (
    <div>
      <h1 className="heading">Subscriptions</h1>
      <Button
        type="primary"
        style={{
          marginBottom: 16,
        }}
        onClick={() => setVisible(true)}
      >
        <PlusOutlined />
        Add New Subscriptions
      </Button>

      <TableForm
        dataSource={dataSource}
        defaultColumns={defaultColumns}
      ></TableForm>
    </div>
  );
}
