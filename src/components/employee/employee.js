import { Table } from "antd";
import React, { useState, useEffect } from "react";
import TableForm from "../table/table";
import "./index.css";
import { Popconfirm, Button } from "antd";
import "antd/dist/antd.css";
import {
  SearchOutlined,
  DeleteOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { PlusOutlined } from "@ant-design/icons";


export default function Employee() {
  const [data,setData] = useState();
  async function getData(
    url = "http://localhost:8080/BDS/employee",
  ){
    const response = await fetch(url, {
      body: JSON.stringify(data),
    });
    return response.json();
  }
  useEffect(() => {
    getData().then((data) => {
      setData(data);
    });
  },[])
  const dataSource = data;
  const [visible, setVisible] = useState(false);
  const defaultColumns = [
    {
      render: (_, record) =>
        dataSource.length >= 1 ? (
          <Popconfirm title="Sure to view?">
            <SearchOutlined />
          </Popconfirm>
        ) : null,
      width: "1%",
    },
    {
      render: (_, record) =>
        dataSource.length >= 1 ? (
          <Popconfirm title="Sure to edit?">
            <EditOutlined />
          </Popconfirm>
        ) : null,
      width: "1%",
    },
    {
      render: (_, record) =>
        dataSource.length >= 1 ? (
          <Popconfirm title="Sure to delete?">
            <DeleteOutlined />
          </Popconfirm>
        ) : null,
      width: "1%",
    },
    {
      title: "ID",
      dataIndex: "employeeID",
      width:"3%"
    },
    {
      title: "FirstName",
      dataIndex: "firstName",
    },
    {
      title: "LastName",
      dataIndex: "lastName",
    },
    {
      title: "Title",
      dataIndex: "title",
    },
    {
      title: "Title Of Courtesy",
      dataIndex: "titleOfCourtesy",
    },
    {
      title: "Birth Date",
      dataIndex: "birthDate",
    },
    {
      title: "Hire Date",
      dataIndex: "hireDate",
    },
    {
      title: "Address",
      dataIndex: "address",
    },
    {
      title: "City",
      dataIndex: "city",
    },
    {
      title: "Region",
      dataIndex: "region",
    },
    {
      title: "Postal Code",
      dataIndex: "postalCode",
    },
    {
      title: "Country",
      dataIndex: "country",
    },
    {
      title: "Home Phone",
      dataIndex: "homePhone",
    },
    {
      title: "Extension",
      dataIndex: "extension",
    },
    {
      title: "Photo",
      dataIndex: "photo",
    },
    {
      title: "Reports To",
      dataIndex: "reportsTo",
    },
    {
      title: "Username",
      dataIndex: "username",
    },
    {
      title: "Password",
      dataIndex: "password",
    },
    {
      title: "Email",
      dataIndex: "email",
    },
    {
      title: "Activated",
      dataIndex: "email",
    },
    {
      title: "User Level",
      dataIndex: "userLevel",
    },
  ];

  return (
    <div>
      <h1 className="heading">Khách hàng</h1>
      <Button
        type="primary"
        style={{
          marginBottom: 16,
        }}
        onClick={() => setVisible(true)}
      >
        <PlusOutlined />
        Add New Employee
      </Button>
      <TableForm
        dataSource={dataSource}
        defaultColumns={defaultColumns}
        className="table-fluid"
        ></TableForm>
    </div>
  );
}
