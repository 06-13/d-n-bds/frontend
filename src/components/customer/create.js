import React, { useState } from "react";
import Modal from "antd/lib/modal/Modal";
import { Input } from "antd";

export default function Create({ trangThai, callbackToggle }) {
  const [data, setData] = useState({
    contactName: "",
    contactTitle: "",
    address: "",
    mobile:"",
    email:"",
    note:"",
    createBy:"",
    updateBy:"",
  });
  return (
    <Modal
      title="Tạo mới người dùng"
      centered
      visible={trangThai}
      onOk={() => {
        console.log(data);
        if (!data.contactName || !data.contactTitle || !data.address || !data.mobile || !data.email || !data.note || !data.createBy || !data.updateBy ) {
          alert("Cần nhập đầy đủ dữ liệu");
        } else {
          let response = fetch("http://localhost:8080/BDS/customer", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
          })
            .then((data) => data.json())
            .then((data) => {
              alert("Tạo mới thành công")
              callbackToggle(false);
            });
        }
      }}
      onCancel={() => callbackToggle(false)}
      width={1000}
    >
      <h3>Tên</h3>
      <Input
        placeholder="Tên ..."
        onChange={(event) => (data.contactName = event.target.value)}
      ></Input>
      <h3>Thông tin liên lạc</h3>
      <Input
        placeholder="Thông tin liên lạc ..."
        onChange={(event) => (data.contactTitle = event.target.value)}
      ></Input>
      <h3>Địa chỉ</h3>
      <Input
        placeholder="Địa chỉ ..."
        onChange={(event) => (data.address = event.target.value)}
      ></Input>
      <h3>Số điện thoại</h3>
      <Input
        placeholder="Số điện thoại ..."
        onChange={(event) => (data.mobile = event.target.value)}
      ></Input>
      <h3>Email</h3>
      <Input
        placeholder="Email ..."
        onChange={(event) => (data.email = event.target.value)}
      ></Input>
      <h3>Ghi chú</h3>
      <Input
        placeholder="Ghi chú ..."
        onChange={(event) => (data.note = event.target.value)}
      ></Input>
      <h3>Tạo bởi</h3>
      <Input
        placeholder="Tạo bởi ..."
        onChange={(event) => (data.createBy = event.target.value*1)}
      ></Input>
      <h3>Cập nhật bởi</h3>
      <Input
        placeholder="Cập nhật bởi ..."
        onChange={(event) => (data.updateBy = event.target.value*1)}
      ></Input>
    </Modal>
  );
}
