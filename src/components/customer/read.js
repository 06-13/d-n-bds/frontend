import React from "react";
import Modal from "antd/lib/modal/Modal";
import { Input } from "antd";

export default function Read({ trangThai, callbackToggle, dataRead }) {
  return (
    <Modal
      title="Thông tin nhà thầu xây dựng"
      centered
      visible={trangThai}
      onOk={() => callbackToggle(false)}
      onCancel={() => callbackToggle(false)}
      width={1000}
    >
      <h3>ID</h3>
      <Input value={dataRead.id}></Input>
      <h3>Họ và tên</h3>
      <Input value={dataRead.contactName}></Input>
      <h3>Thông tin liên lạc</h3>
      <Input value={dataRead.contactTitle}></Input>
      <h3>Địa chỉ</h3>
      <Input value={dataRead.address}></Input>
      <h3>Số điện thoại</h3>
      <Input value={dataRead.mobile}></Input>
      <h3>Email</h3>
      <Input value={dataRead.email}></Input>
      <h3>Ghi chú</h3>
      <Input value={dataRead.note}></Input>
      <h3>Tạo bởi</h3>
      <Input value={dataRead.createBy}></Input>
      <h3>Cập nhật bởi</h3>
      <Input value={dataRead.updateBy}></Input>
      <h3>Ngày tạo</h3>
      <Input value={dataRead.createDate}></Input>
      <h3>Ngày cập nhật</h3>
      <Input value={dataRead.updateDate}></Input>
    </Modal>
  );
}
