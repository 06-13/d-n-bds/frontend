import React, { useEffect, useState } from "react";
import Modal from "antd/lib/modal/Modal";
import { Input } from "antd";
import "antd/dist/antd.css";

export default function Edit({ trangThai, callbackToggle, dataEdit }) {
  const [data, setData] = useState({
    contactName: "",
    contactTitle: "",
    address: "",
    mobile:"",
    email:"",
    note:"",
    createBy:"",
    updateBy:"",
  });
  return (
    <Modal
      title="Chỉnh sửa thông tin người dùng"
      centered
      visible={trangThai}
      onOk={() => {
        console.log(data);
        if (!data.contactName || !data.contactTitle || !data.address || !data.mobile || !data.email || !data.note || !data.createBy || !data.updateBy ) {
          alert("Cần nhập đầy đủ dữ liệu");
        } else {
          let response = fetch(
            "http://localhost:8090/customer/" + dataEdit.id,
            {
              method: "PUT",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(data),
            }
          )
            .then((data) => data.json())
            .then((data) => {
              alert("Cập nhật thành công")
              callbackToggle(false);
            });
        }
      }}
      onCancel={() => callbackToggle(false)}
      width={1000}
    > 
      <h3>ID</h3>
      <Input
        disabled
        placeholder={dataEdit.id}
      ></Input>
      <h3>Tên</h3>
      <Input
        placeholder={dataEdit.contactName}
        onChange={(event) => (data.contactName = event.target.value)}
      ></Input>
      <h3>Thông tin liên lạc</h3>
      <Input
        placeholder={dataEdit.contactTitle}
        onChange={(event) => (data.contactTitle = event.target.value)}
      ></Input>
      <h3>Địa chỉ</h3>
      <Input
        placeholder={dataEdit.address}
        onChange={(event) => (data.address = event.target.value)}
      ></Input>
      <h3>Số điện thoại</h3>
      <Input
        placeholder={dataEdit.mobile}
        onChange={(event) => (data.mobile = event.target.value)}
      ></Input>
      <h3>Email</h3>
      <Input
        placeholder={dataEdit.email}
        onChange={(event) => (data.email = event.target.value)}
      ></Input>
      <h3>Ghi chú</h3>
      <Input
        placeholder={dataEdit.note}
        onChange={(event) => (data.note = event.target.value)}
      ></Input>
      <h3>Tạo bởi</h3>
      <Input
        placeholder={dataEdit.createBy}
        onChange={(event) => (data.createBy = event.target.value)}
      ></Input>
      <h3>Cập nhật bởi</h3>
      <Input
        placeholder={dataEdit.updateBy}
        onChange={(event) => (data.updateBy = event.target.value)}
      ></Input>
    </Modal>
  );
}
