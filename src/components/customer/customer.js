import React, { useState, useEffect } from "react";
import TableForm from "../table/table";
import "./index.css";
import { Button , Popconfirm } from "antd";
import "antd/dist/antd.css";
import {
  SearchOutlined,
  DeleteOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { PlusOutlined } from "@ant-design/icons";
import Create from "./create";
import Read from "./read";
import Edit from "./edit";

export default function Customer() {
  const [data, setData] = useState();
  const dataSource = data;
  const [visible, setVisible] = useState(false);
  const [visibleRead, setVisibleRead] = useState(false);
  const [visibleEdit, setVisibleEdit] = useState(false);
  const [dataRead, setDataRead] = useState([]);
  const [dataEdit, setDataEdit] = useState([]);

  const defaultColumns = [
    {
      render: (_, record) =>
        dataSource.length >= 1 ? (
          <SearchOutlined
            onClick={() => {
              setDataRead(record);
              setVisibleRead(true);
            }}
          ></SearchOutlined>
        ) : null,
      width: "1%",
    },
    {
      render: (_, record) =>
        dataSource.length >= 1 ? (
          <EditOutlined
            onClick={() => {
              setVisibleEdit(true);
              setDataEdit(record);
            }}
          />
        ) : null,
      width: "1%",
    },
    {
      render: (_, record) =>
        dataSource.length >= 1 ? (
          <Popconfirm title="Đồng ý xoá ?" onConfirm={() => {fetch("http://localhost:8080/BDS/customer  /" + record.id, {
            method: "DELETE",
          }).then((data) => {
            alert("Xoá người dùng thành công");
          })}}>
          <DeleteOutlined
          />
          </Popconfirm>
        ) : null,
      width: "1%",
    },
    {
      title: "ID",
      dataIndex: "id",
    },
    {
      title: "Họ và tên",
      dataIndex: "contactName",
    },
    {
      title: "Thông tin liên lạc",
      dataIndex: "contactTitle",
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
    },
    {
      title: "Số điện thoại",
      dataIndex: "mobile",
    },
    {
      title: "Email",
      dataIndex: "email ",
    },
    {
      title: "Ghi chú",
      dataIndex: "note",
    },
    {
      title: "Tạo bởi ",
      dataIndex: "createBy",
    },
    {
      title: "Cập hật bởi",
      dataIndex: "updateBy",
    },
    {
      title: "Ngày tạo",
      dataIndex: "createDate",
    },
    {
      title: "Ngày cập nhật",
      dataIndex: "updateDate",
    },
    
  ];

  async function getData(url = "http://localhost:8080/BDS/customer") {
    const response = await fetch(url, {
      method:'GET'
    });
    return response.json();
  }

  useEffect(() => {
    getData().then((data) => {
      setData(data);
    });
  },);

  return (
    <div>
      <h1 className="heading">Người dùng</h1>
      <Button
        type="primary"
        style={{
          marginBottom: 16,
        }}
        onClick={() => setVisible(true)}
      >
        <PlusOutlined />
        Thêm mới người dùng
      </Button>
      <Create
        trangThai={visible}
        callbackToggle={(state) => setVisible(state)}
      ></Create>
      <Read
        trangThai={visibleRead}
        callbackToggle={(state) => setVisibleRead(state)}
        dataRead={dataRead}
      ></Read>
      <Edit
        trangThai={visibleEdit}
        callbackToggle={(state) => setVisibleEdit(state)}
        dataEdit={dataEdit}
      ></Edit>
      <TableForm
        dataSource={dataSource}
        defaultColumns={defaultColumns}
      ></TableForm>
    </div>
  );
}
