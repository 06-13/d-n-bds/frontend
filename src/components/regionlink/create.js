import React, { useState } from "react";
import Modal from "antd/lib/modal/Modal";
import { Input } from "antd";

export default function Create({ trangThai, callbackToggle }) {
  const [data, setData] = useState({
    name: "",
    description: "",
    photo: "",
    address:"",
    lat:"",
    lng:"",
  });
  return (
    <Modal
      title="Tạo mới kết nối vùng"
      centered
      visible={trangThai}
      onOk={() => {
        console.log(data);
        if (!data.name || !data.description || !data.photo || !data.address || !data.lat || !data.lng) {
          alert("Cần nhập đầy đủ dữ liệu");
        } else {
          let response = fetch("http://localhost:8080/BDS/regionlink", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
          })
            .then((data) => data.json())
            .then((data) => {
              alert("Tạo mới thành công")
              callbackToggle(false);
            });
        }
      }}
      onCancel={() => callbackToggle(false)}
      width={1000}
    >
      <h3>Tên</h3>
      <Input
        placeholder="Tên ..."
        onChange={(event) => (data.name = event.target.value)}
      ></Input>
      <h3>Mô tả</h3>
      <Input
        placeholder="Mô tả ..."
        onChange={(event) => (data.description = event.target.value)}
      ></Input>
      <h3>Hình ảnh</h3>
      <Input
        placeholder="Hình ảnh ..."
        onChange={(event) => (data.photo = event.target.value)}
      ></Input>
      <h3>Địa chỉ</h3>
      <Input
        placeholder="Địa chỉ ..."
        onChange={(event) => (data.address = event.target.value)}
      ></Input>
      <h3>Vĩ độ</h3>
      <Input
        placeholder="Vĩ độ ..."
        onChange={(event) => (data.lat = event.target.value)*1}
      ></Input>
      <h3>Kinh độ</h3>
      <Input
        placeholder="Kinh độ ..."
        onChange={(event) => (data.lng = event.target.value)*1}
      ></Input>
      
    </Modal>
  );
}
