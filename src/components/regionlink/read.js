import React, { useEffect, useState } from "react";
import Modal from "antd/lib/modal/Modal";
import { Input } from "antd";

export default function Read({ trangThai, callbackToggle, dataRead }) {
  return (
    <Modal
      title="Thông tin nhà đầu tư"
      centered
      visible={trangThai}
      onOk={() => callbackToggle(false)}
      onCancel={() => callbackToggle(false)}
      width={1000}
    >
      <h3>ID</h3>
      <Input value={dataRead.id}></Input>
      <h3>Tên</h3>
      <Input value={dataRead.name}></Input>
      <h3>Mô tả</h3>
      <Input value={dataRead.description}></Input>
      <h3>Hình ảnh</h3>
      <Input value={dataRead.photo}></Input>
      <h3>Địa chỉ</h3>
      <Input value={dataRead.address}></Input>
      <h3>Vĩ độ</h3>
      <Input value={dataRead.lat}></Input>
      <h3>Kinh độ</h3>
      <Input value={dataRead.lng}></Input>
    </Modal>
  );
}
