import React, { useEffect, useState } from "react";
import Modal from "antd/lib/modal/Modal";
import { Input, Form, Alert } from "antd";

export default function Create({ trangThai, callbackToggle }) {
  const [data, setData] = useState({
    address: "",
    lat: "",
    lng: "",
  });
  return (
    <Modal
      title="Tạo mới địa chỉ toạ độ"
      centered
      visible={trangThai}
      onOk={() => {
        if (!data.address || !data.lat || !data.lat) {
          alert("Cần nhập đầy đủ dữ liệu");
        } else {
          let response = fetch("http://localhost:8080/BDS/address", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
          })
            .then((data) => data.json())
            .then((data) => {
              alert("Tạo mới toạ độ địa chỉ thành công")
            });
          callbackToggle(false);
        }
      }}
      onCancel={() => callbackToggle(false)}
      width={1000}
    >
      <h3>Địa chỉ</h3>
      <Input
        placeholder="Địa chỉ ..."
        onChange={(event) => (data.address = event.target.value)}
      ></Input>
      <h3>Kinh độ</h3>
      <Input
        placeholder="Kinh độ ..."
        onChange={(event) => (data.lat = event.target.value * 1)}
      ></Input>
      <h3>Vĩ độ</h3>
      <Input
        placeholder="Vĩ độ ..."
        onChange={(event) => (data.lng = event.target.value * 1)}
      ></Input>
    </Modal>
  );
}
