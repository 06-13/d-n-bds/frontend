import React, { useEffect, useState } from "react";
import Modal from "antd/lib/modal/Modal";
import { Input } from "antd";

export default function Read({ trangThai, callbackToggle, dataRead }) {
  return (
    <Modal
      title="Thông tin  địa chỉ toạ độ"
      centered
      visible={trangThai}
      onOk={() => callbackToggle(false)}
      onCancel={() => callbackToggle(false)}
      width={1000}
    >
      <h3>ID</h3>
      <Input value={dataRead.id}></Input>
      <h3>Mã thành phố</h3>
      <Input value={dataRead.provinceId}></Input>
      <h3>Mã quận huyện</h3>
      <Input value={dataRead.districtId}></Input>
      <h3>Mã xã phường</h3>
      <Input value={dataRead.wardId}></Input>
      <h3>Mã đường phố</h3>
      <Input value={dataRead.streetId}></Input>
      <h3>Tên dự án</h3>
      <Input value={dataRead.name}></Input>
      <h3>Địa chỉ</h3>
      <Input value={dataRead.address}></Input>
      <h3>Mô tả</h3>
      <Input value={dataRead.description}></Input>
      <h3>Diện tích</h3>
      <Input value={dataRead.acreage}></Input>
      <h3>Khu vực xây dựng</h3>
      <Input value={dataRead.constructArea}></Input>
      <h3>Số toà</h3>
      <Input value={dataRead.numBlock}></Input>
      <h3>Số tầng</h3>
      <Input value={dataRead.numFloors}></Input>
      <h3>Số căn hộ</h3>
      <Input value={dataRead.numApartment}></Input>
      <h3>Slogan</h3>
      <Input value={dataRead.slogan}></Input>
      <h3>Khu vực căn hộ</h3>
      <Input value={dataRead.apartmentArea}></Input>
      <h3>Nhà đầu tư</h3>
      <Input value={dataRead.investor}></Input>
      <h3>Đơn vị thi công</h3>
      <Input value={dataRead.constructionContractor}></Input>
      <h3>Đơn vị thiết kế</h3>
      <Input value={dataRead.designUnit}></Input>
      <h3>Tiện ích</h3>
      <Input value={dataRead.utilities}></Input>
      <h3>Kết nối</h3>
      <Input value={dataRead.regionLink}></Input>
      <h3>Hình ảnh</h3>
      <Input value={dataRead.photo}></Input>
      <h3>Kinh độ</h3>
      <Input value={dataRead.lat}></Input>
      <h3>Vĩ độ</h3>
      <Input value={dataRead.lng}></Input>
    </Modal>
  );
}
