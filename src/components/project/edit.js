import React, { useState } from "react";
import Modal from "antd/lib/modal/Modal";
import { Input } from "antd";
import "antd/dist/antd.css";

export default function Edit({ trangThai, callbackToggle, dataEdit }) {
  const [data, setData] = useState({
    address: "",
    lat: "",
    lng: "",
  });
  return (
    <Modal
      title="Chỉnh sửa địa chỉ toạ độ"
      centered
      visible={trangThai}
      onOk={() => {
        if (!data.address || !data.lat || !data.lat) {
          alert("Cần nhập đầy đủ dữ liệu");
        } else {
          let response = fetch(
            "http://localhost:8080/BDS/address/" + dataEdit.id,
            {
              method: "PUT",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(data),
            }
          )
            .then((data) => data.json())
            .then((data) => {
              alert("Cập nhật toạ độ địa chỉ thành công")
              callbackToggle(false);
            });
        }
      }}
      onCancel={() => callbackToggle(false)}
      width={1000}
    >
      <h3>ID</h3>
      <Input
      placeholder={dataEdit.id}
      disabled></Input>
      <h3>Mã thành phố</h3>
      <Input
        placeholder={dataEdit.provinceId}
        onChange={(event) => (data.provinceId = event.target.value)}
      ></Input>
      <h3>Mã quận huyện</h3>
      <Input
        placeholder={dataEdit.districtId}
        onChange={(event) => (data.districtId = event.target.value)}
      ></Input>
      <h3>Mã xã phường</h3>
      <Input
        placeholder={dataEdit.wardId}
        onChange={(event) => (data.wardId = event.target.value)}
      ></Input>
      <h3>Mã đường </h3>
      <Input
        placeholder={dataEdit.streetId}
        onChange={(event) => (data.streetId = event.target.value)}
      ></Input>
      <h3>Tên dự án</h3>
      <Input
        placeholder={dataEdit.name}
        onChange={(event) => (data.name = event.target.value)}
      ></Input>
      <h3>Địa chỉ</h3>
      <Input
        placeholder={dataEdit.address}
        onChange={(event) => (data.address = event.target.value)}
      ></Input>
      <h3>Mô tả</h3>
      <Input
        placeholder={dataEdit.description}
        onChange={(event) => (data.description = event.target.value)}
      ></Input>
      <h3>Diện tích</h3>
      <Input
        placeholder={dataEdit.acreage}
        onChange={(event) => (data.acreage = event.target.value)}
      ></Input>
      <h3>Khu vực xây dựng</h3>
      <Input
        placeholder={dataEdit.constructArea}
        onChange={(event) => (data.constructArea = event.target.value)}
      ></Input>
      <h3>Số toà nhà</h3>
      <Input
        placeholder={dataEdit.numBlock}
        onChange={(event) => (data.numBlock = event.target.value)}
      ></Input>
      <h3>Số tầng</h3>
      <Input
        placeholder={dataEdit.numFloors}
        onChange={(event) => (data.numFloors = event.target.value)}
      ></Input>
      <h3>Số căn hộ</h3>
      <Input
        placeholder={dataEdit.numApartment}
        onChange={(event) => (data.numApartment = event.target.value)}
      ></Input>
      <h3>Slogan</h3>
      <Input
        placeholder={dataEdit.slogan}
        onChange={(event) => (data.slogan = event.target.value)}
      ></Input>
      <h3>Khu vực căn hộ</h3>
      <Input
        placeholder={dataEdit.addrapartmentAreaess}
        onChange={(event) => (data.apartmentArea = event.target.value)}
      ></Input>
      <h3>Nhà đầu tư</h3>
      <Input
        placeholder={dataEdit.investor}
        onChange={(event) => (data.investor = event.target.value)}
      ></Input>
      <h3>Đơn vị thi công</h3>
      <Input
        placeholder={dataEdit.constructionContractor}
        onChange={(event) => (data.constructionContractor = event.target.value)}
      ></Input>
      <h3>Đơn vị thiết kế</h3>
      <Input
        placeholder={dataEdit.designUnit}
        onChange={(event) => (data.designUnit = event.target.value)}
      ></Input>
      <h3>Tiện ích</h3>
      <Input
        placeholder={dataEdit.utilities}
        onChange={(event) => (data.utilities = event.target.value)}
      ></Input>
      <h3>Kết nối</h3>
      <Input
        placeholder={dataEdit.regionLink}
        onChange={(event) => (data.regionLink = event.target.value)}
      ></Input>
      <h3>Hình ảnh</h3>
      <Input
        placeholder={dataEdit.photo}
        onChange={(event) => (data.photo = event.target.value)}
      ></Input>
      <h3>Kinh độ</h3>
      <Input
        placeholder={dataEdit.lat}
        onChange={(event) => (data.lat = event.target.value)}
      ></Input>
      <h3>Vĩ độ</h3>
      <Input
        placeholder={dataEdit.lng}
        onChange={(event) => (data.lng = event.target.value)}
      ></Input>
    </Modal>
  );
}
