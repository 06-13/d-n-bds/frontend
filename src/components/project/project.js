import React, { useState, useEffect } from "react";
import TableForm from "../table/table";
import "./index.css";
import { Button , Popconfirm } from "antd";
import "antd/dist/antd.css";
import {
  SearchOutlined,
  DeleteOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { PlusOutlined } from "@ant-design/icons";
import Create from "./create";
import Read from "./read";
import Edit from "./edit";

export default function Project() {

  const [data, setData] = useState();
  const dataSource = data;
  const [visible, setVisible] = useState(false);
  const [visibleRead, setVisibleRead] = useState(false);
  const [visibleEdit, setVisibleEdit] = useState(false);
  const [dataRead, setDataRead] = useState([]);
  const [dataEdit, setDataEdit] = useState([]);
  const [provinceData,SetProvinceData] = useState([]);
  const [districtData,SetDistrictData] = useState([]);
  const [wardData,SetWardData] = useState([]);
  const [streetData,SetStreetData] = useState([]);

  const defaultColumns = [
    {
      render: (_, record) =>
        dataSource.length >= 1 ? (
          <SearchOutlined
            onClick={() => {
              setDataRead(record);
              setVisibleRead(true);
            }}
          ></SearchOutlined>
        ) : null,
      width: "1%",
    },
    {
      render: (_, record) =>
        dataSource.length >= 1 ? (
          <EditOutlined
            onClick={() => {
              setVisibleEdit(true);
              setDataEdit(record);
            }}
          />
        ) : null,
      width: "1%",
    },
    {
      render: (_, record) =>
        dataSource.length >= 1 ? (
          <Popconfirm title="Đồng ý xoá ?" onConfirm={() => {fetch("http://localhost:8080/BDS/project/" + record.id, {
            method: "DELETE",
          }).then((data) => {
            alert("Xoá toạ độ địa chỉ thành công");
          })}}>
          <DeleteOutlined
          />
          </Popconfirm>
        ) : null,
      width: "1%",
    },
    {
      title: "ID",
      dataIndex: "id",
      width:"3%"
    },
    {
      title: "Thành phố",
      dataIndex: "provinceId",
      width:"5%",
      render: (_,record) => {
        for(var i = 0;i<provinceData.length;i++){
          if(provinceData[i].id === record.provinceId){
            return (provinceData[i].name)
          }
        }
      }
    },
    {
      title: "Quận huyện",
      dataIndex: "districtId",
      width:"5%",
      render: (_,record) => {
        for(var i = 0;i<districtData.length;i++){
          if(districtData[i].id === record.districtId){
            return (districtData[i].name)
          }
        }
      }
    },
    {
      title: "Xã phường",
      dataIndex: "wardId",
      width:"5%",
      render: (_,record) => {
        for(var i = 0;i<wardData.length;i++){
          if(wardData[i].id === record.wardId){
            return (wardData[i].name)
          }
        }
      }
    },
    {
      title: "Đường",
      dataIndex: "streetId",
      width:"5%",
      render: (_,record) => {
        for(var i = 0;i<streetData.length;i++){
          if(streetData[i].id === record.streetId){
            return (streetData[i].name)
          }
        }
      }
    },
    {
      title: "Tên dự án",
      dataIndex: "name",
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
    },
    {
      title: "Mô tả",
      dataIndex: "description",
    },
    {
      title: "Diện tích",
      dataIndex: "acreage",
    },
    {
      title: "Khu vực xây dựng",
      dataIndex: "constructArea",
    },
    {
      title: "Số toà nhà",
      dataIndex: "numBlock",
    },
    {
      title: "Số tầng",
      dataIndex: "numFloors",
    },
    {
      title: "Số căn hộ",
      dataIndex: "numApartment",
    },
    {
      title: "Slogan",
      dataIndex: "slogan",
    },
    {
      title: "Khu vực căn hộ",
      dataIndex: "apartmentArea",
    },
    {
      title: "Nhà đầu tư",
      dataIndex: "investor",
    },
    {
      title: "Đơn vị thi công",
      dataIndex: "constructionContractor",
    },
    {
      title: "Đơn vị thiết kế",
      dataIndex: "designUnit",
    },
    {
      title: "Tiện ích",
      dataIndex: "utilities",
    },
    {
      title: "Kết nối",
      dataIndex: "regionLink",
    },
    {
      title: "Hình ảnh",
      dataIndex: "photo",
    },
    {
      title: "Kinh độ",
      dataIndex: "lng",
    },
    {
      title: "Vĩ độ",
      dataIndex: "lat",
    },
    
  ];

  async function getData(url = "http://localhost:8080/BDS/project") {
    const response = await fetch(url, {
      method:'GET'
    });
    return response.json();
  }
  async function getProvinceData(url = "http://localhost:8080/BDS/province") {
    const response = await fetch(url, {
      method:'GET'
    });
    return response.json();
  }
  async function getDistrictData(url = "http://localhost:8080/BDS/district") {
    const response = await fetch(url, {
      method:'GET'
    });
    return response.json();
  }
  async function getWardData(url = "http://localhost:8080/BDS/ward") {
    const response = await fetch(url, {
      method:'GET'
    });
    return response.json();
  }
  async function getStreetData(url = "http://localhost:8080/BDS/street") {
    const response = await fetch(url, {
      method:'GET'
    });
    return response.json();
  }
  useEffect(() => {
    getProvinceData().then((data) => {
      SetProvinceData(data)
    })
  },[]);
  useEffect(() => {
    getDistrictData().then((data) => {
      SetDistrictData(data)
    })
  },[]);
  useEffect(() => {
    getWardData().then((data) => {
      SetWardData(data)
    })
  },[]);
  useEffect(() => {
    getStreetData().then((data) => {
      SetStreetData(data)
    })
  },[]);
  useEffect(() => {
    getData().then((data) => {
      setData(data);
    });
  },);

  return (
    <div>
      <h1 className="heading">Dự án xây dựng</h1>
      <Button
        type="primary"
        style={{
          marginBottom: 16,
        }}
        onClick={() => setVisible(true)}
      >
        <PlusOutlined />
        Thêm mới dự án xây dựng
      </Button>
      <Create
        trangThai={visible}
        callbackToggle={(state) => setVisible(state)}
      ></Create>
      <Read
        trangThai={visibleRead}
        callbackToggle={(state) => setVisibleRead(state)}
        dataRead={dataRead}
      ></Read>
      <Edit
        trangThai={visibleEdit}
        callbackToggle={(state) => setVisibleEdit(state)}
        dataEdit={dataEdit}
      ></Edit>
      <TableForm
        dataSource={dataSource}
        defaultColumns={defaultColumns}
      ></TableForm>
    </div>
  );
}
