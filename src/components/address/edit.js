import React, { useEffect, useState } from "react";
import Modal from "antd/lib/modal/Modal";
import { Input } from "antd";
import "antd/dist/antd.css";

export default function Edit({ trangThai, callbackToggle, dataEdit }) {
  const [data, setData] = useState({
    address: "",
    lat: "",
    lng: "",
  });
  return (
    <Modal
      title="Chỉnh sửa thông tin địa chỉ toạ độ"
      centered
      visible={trangThai}
      onOk={() => {
        if (!data.address || !data.lat || !data.lat) {
          alert("Cần nhập đầy đủ dữ liệu");
        } else {
          let response = fetch(
            "http://localhost:8080/BDS/address/" + dataEdit.id,
            {
              method: "PUT",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(data),
            }
          )
            .then((data) => data.json())
            .then((data) => {
              alert("Cập nhật toạ độ địa chỉ thành công")
              callbackToggle(false);
            });
        }
      }}
      onCancel={() => callbackToggle(false)}
      width={1000}
    >
      <h3>ID</h3>
      <Input
      placeholder={dataEdit.id}
      disabled></Input>
      <h3>Địa chỉ</h3>
      <Input
        placeholder={dataEdit.address}
        onChange={(event) => (data.address = event.target.value)}
      ></Input>
      <h3>Kinh độ</h3>
      <Input
        placeholder={dataEdit.lat}
        onChange={(event) => (data.lat = event.target.value)}
      ></Input>
      <h3>Vĩ độ</h3>
      <Input
        placeholder={dataEdit.lng}
        onChange={(event) => (data.lng = event.target.value)}
      ></Input>
    </Modal>
  );
}
