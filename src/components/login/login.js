import React , { useState } from "react";
import "antd/dist/antd.css";
import { Button , Form, Input } from "antd";
import axios from 'axios';
import Modal from "antd/lib/modal/Modal";

const Login = ({callBackSetToken}) => { 
    const [visible, setVisible] = useState(false);

    const onFinish = (values) => {
    
        axios.post('http://localhost:8900/login',values)
        .then(function (response) {
        callBackSetToken(response)

    })};

    const onFinishRegister = (values) => {
        axios.post('http://localhost:8900/register',values)
        .then(function (response) {
            console.log(response);
            alert("Đăng ký thành công !")
            setVisible(false);
        })
    };

    const onFinishFailed = (errorInfo) => {
        console.log("Failed:", errorInfo);
    };

    const onFinishFailedRegister = (errorInfo) => {
        console.log("Failed:", errorInfo);
    };
  return (
    <>
    <Form
      name="basic"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 16,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Username"
        name="username"
        rules={[
          {
            required: true,
            message: "Please input your username!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: "Please input your password!",
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Button type="primary" htmlType="submit">
          Đăng nhập
        </Button>
        <Button type="danger"  style={{'marginLeft': '40px'}} onClick={() => setVisible(true)}>Đăng ký</Button>
      </Form.Item>
    </Form>
    <Modal
    title="Chỉnh sửa thông tin địa chỉ toạ độ"
    centered
    visible={visible}
    onOk={() => setVisible(false)}
    onCancel={() => {setVisible(false)}}
    width={1000}
  >
    <Form
      name="basic"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 16,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinishRegister}
      onFinishFailed={onFinishFailedRegister}
      autoComplete="off"
    >
      <Form.Item
        label="Username"
        name="username"
        rules={[
          {
            required: true,
            message: "Please input your username!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: "Please input your password!",
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Button type="primary" htmlType="submit">
          Đăng ký
        </Button>
      </Form.Item>
    </Form>
  </Modal>
</>
  );
};

export default Login;
