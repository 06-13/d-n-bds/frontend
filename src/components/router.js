import React from "react";
import { Route, Routes } from "react-router-dom";
import Employee from "./employee/employee";
import Customer from "./customer/customer";
import Address from "./address/address";
import MasterLayout from "./masterlayout.js/masterlayout";
import Contractor from "./contractor/contractor";
import DesignUnit from "./designunit/designunit";
import Investor from "./investor/investor";
import RegionLink from "./regionlink/regionlink";
import Utilities from "./utilities/utilities";
import Subscriptions from "./employee/subscriptions";
import Project from "./project/project";
import Ward from "./employee/ward";

export default function MainPage() {
  return (
    <Routes>
      <Route path="/employee" element={<Employee></Employee>}></Route>
      <Route path="/customer" element={<Customer></Customer>}></Route>
      <Route path="/" element={<Address></Address>}></Route>
      <Route
        path="/masterlayout"
        element={<MasterLayout></MasterLayout>}
      ></Route>
      <Route path="/contractor" element={<Contractor></Contractor>}></Route>
      <Route path="/designunit" element={<DesignUnit></DesignUnit>}></Route>
      <Route path="/investor" element={<Investor></Investor>}></Route>
      <Route path="/regionlink" element={<RegionLink></RegionLink>}></Route>
      <Route path="/utilities" element={<Utilities></Utilities>}></Route>
      <Route
        path="/subscriptions"
        element={<Subscriptions></Subscriptions>}
      ></Route>
      <Route path="/project" element={<Project></Project>}></Route>
      <Route path="/ward" element={<Ward></Ward>}></Route>
      <Route path="/create-address" element={<Ward></Ward>}></Route>
    </Routes>
  );
}
