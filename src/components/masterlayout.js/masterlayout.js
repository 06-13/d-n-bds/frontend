import { Table } from "antd";
import React, { useState, useEffect } from "react";
import TableForm from "../table/table";
import "./index.css";
import { Popconfirm, Button } from "antd";
import "antd/dist/antd.css";
import {
  SearchOutlined,
  DeleteOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { PlusOutlined } from "@ant-design/icons";
import Read from "./read";
import Create from "./create";
import Edit from "./edit";

export default function MasterLayout() {
  const [data, setData] = useState();
  const dataSource = data;
  const [visible, setVisible] = useState(false);
  const [visibleRead, setVisibleRead] = useState(false);
  const [visibleEdit, setVisibleEdit] = useState(false);
  const [dataRead, setDataRead] = useState([]);
  const [dataEdit, setDataEdit] = useState([]);


  const defaultColumns = [
    {
      render: (_, record) =>
        dataSource.length >= 1 ? (
          <SearchOutlined
            onClick={() => {
              setDataRead(record);
              setVisibleRead(true);
            }}
          ></SearchOutlined>
        ) : null,
      width: "1%",
    },
    {
      render: (_, record) =>
        dataSource.length >= 1 ? (
          <EditOutlined
            onClick={() => {
              setVisibleEdit(true);
              setDataEdit(record);
            }}
          />
        ) : null,
      width: "1%",
    },
    {
      render: (_, record) =>
        dataSource.length >= 1 ? (
          <Popconfirm title="Đồng ý xoá ?" onConfirm={() => {fetch("http://localhost:8080/BDS/masterlayout/" + record.id, {
            method: "DELETE",
          }).then((data) => {
            alert("Xoá toạ độ địa chỉ thành công");
          })}}>
          <DeleteOutlined
          />
          </Popconfirm>
        ) : null,
      width: "1%",
    },
    {
      title: "ID",
      dataIndex: "id",
    },
    {
      title: "Tên",
      dataIndex: "name",
    },
    {
      title: "Mô tả",
      dataIndex: "description",
    },
    {
      title: "Dự án",
      dataIndex: "projectId",
    },
    {
      title: "Diện tích",
      dataIndex: "acreage",
    },
    {
      title: "Danh sách căn hộ",
      dataIndex: "apartmentList",
    },
    {
      title: "Hình ảnh",
      dataIndex: "photo",
    },
    {
      title: "Ngày tạo",
      dataIndex: "dateCreate",
    },
    {
      title: "Ngày cập nhật",
      dataIndex: "dateUpdate",
    },
  ];

  async function getData(url = "http://localhost:8080/BDS/masterlayout") {
    const response = await fetch(url, {
      method:'GET'
    });
    return response.json();
  }

  useEffect(() => {
    getData().then((data) => {
      setData(data);
    });
  },);

  return (
    <div>
      <h1 className="heading">Mặt bằng tầng</h1>
      <Button
        type="primary"
        style={{
          marginBottom: 16,
        }}
        onClick={() => setVisible(true)}
      >
        <PlusOutlined />
        Thêm mới mặt bằng tầng
      </Button>
      <Create
        trangThai={visible}
        callbackToggle={(state) => setVisible(state)}
      ></Create>
      <Read
        trangThai={visibleRead}
        callbackToggle={(state) => setVisibleRead(state)}
        dataRead={dataRead}
      ></Read>
      <Edit
        trangThai={visibleEdit}
        callbackToggle={(state) => setVisibleEdit(state)}
        dataEdit={dataEdit}
      ></Edit>
      <TableForm
        dataSource={dataSource}
        defaultColumns={defaultColumns}
      ></TableForm>
    </div>
  );
}
