import React, { useEffect, useState } from "react";
import Modal from "antd/lib/modal/Modal";
import { Input } from "antd";
import "antd/dist/antd.css";

export default function Edit({ trangThai, callbackToggle, dataEdit }) {
  const [data, setData] = useState({
    name: "",
    description: "",
    projectId: "",
    acreage:"",
    apartmentList:"",
    photo:"",
  });
  return (
    <Modal
      title="Chỉnh sửa thông tin mặt bằng tầng"
      centered
      visible={trangThai}
      onOk={() => {
        console.log(data);
        if (!data.name || !data.projectId || !data.acreage || !data.apartmentList ) {
          alert("Cần nhập đầy đủ dữ liệu");
        } else {
          let response = fetch(
            "http://localhost:8080/BDS/masterlayout/" + dataEdit.id,
            {
              method: "PUT",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(data),
            }
          )
            .then((data) => data.json())
            .then((data) => {
              alert("Cập nhật mặt bằng tầng thành công")
              callbackToggle(false);
            });
        }
      }}
      onCancel={() => callbackToggle(false)}
      width={1000}
    > 
      <h3>ID</h3>
      <Input
        disabled
        placeholder={dataEdit.id}
      ></Input>
      <h3>Tên</h3>
      <Input
        placeholder={dataEdit.name}
        onChange={(event) => (data.name = event.target.value)}
      ></Input>
      <h3>Mô tả</h3>
      <Input
        placeholder={dataEdit.description}
        onChange={(event) => (data.description = event.target.value)}
      ></Input>
      <h3>Dự án số</h3>
      <Input
        placeholder={dataEdit.projectId}
        onChange={(event) => (data.projectId = event.target.value)*1}
      ></Input>
      <h3>Diện tích</h3>
      <Input
        placeholder={dataEdit.acreage}
        onChange={(event) => (data.acreage = event.target.value)*1}
      ></Input>
      <h3>Hình ảnh</h3>
      <Input
        placeholder={dataEdit.photo}
        onChange={(event) => (data.photo = event.target.value)}
      ></Input>
      <h3>Danh sách căn hộ</h3>
      <Input
        placeholder={dataEdit.apartmentList}
        onChange={(event) => (data.apartmentList = event.target.value)}
      ></Input>
    </Modal>
  );
}
