import React, { useState } from "react";
import Modal from "antd/lib/modal/Modal";
import { Input } from "antd";

export default function Create({ trangThai, callbackToggle }) {
  const [data, setData] = useState({
    name: "",
    description: "",
    projectId: "",
    acreage:"",
    apartmentList:"",
    photo:"",
  });
  return (
    <Modal
      title="Tạo mới mặt bằng tầng"
      centered
      visible={trangThai}
      onOk={() => {
        console.log(data);
        if (!data.name || !data.description || !data.projectId || !data.acreage || !data.apartmentList || !data.photo) {
          alert("Cần nhập đầy đủ dữ liệu");
        } else {
          let response = fetch("http://localhost:8080/BDS/masterlayout", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
          })
            .then((data) => data.json())
            .then((data) => {
              alert("Tạo mới toạ độ địa chỉ thành công")
            });
          callbackToggle(false);
        }
      }}
      onCancel={() => callbackToggle(false)}
      width={1000}
    >
      <h3>Tên</h3>
      <Input
        placeholder="Tên ..."
        onChange={(event) => (data.name = event.target.value)}
      ></Input>
      <h3>Mô tả</h3>
      <Input
        placeholder="Mô tả ..."
        onChange={(event) => (data.description = event.target.value)}
      ></Input>
      <h3>Dự án số</h3>
      <Input
        placeholder="Dự án số ..."
        onChange={(event) => (data.projectId = event.target.value)*1}
      ></Input>
      <h3>Diện tích</h3>
      <Input
        placeholder="Diện tích ..."
        onChange={(event) => (data.acreage = event.target.value)*1}
      ></Input>
      <h3>Hình ảnh</h3>
      <Input
        placeholder="Diện tích ..."
        onChange={(event) => (data.photo = event.target.value)*1}
      ></Input>
      <h3>Danh sách căn hộ</h3>
      <Input
        placeholder="Danh sách căn hộ ..."
        onChange={(event) => (data.apartmentList = event.target.value)}
      ></Input>
    </Modal>
  );
}
