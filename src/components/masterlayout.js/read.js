import React, { useEffect, useState } from "react";
import Modal from "antd/lib/modal/Modal";
import { Input } from "antd";

export default function Read({ trangThai, callbackToggle, dataRead }) {
  return (
    <Modal
      title="Thông tin  mặt bằng tầng"
      centered
      visible={trangThai}
      onOk={() => callbackToggle(false)}
      onCancel={() => callbackToggle(false)}
      width={1000}
    >
      <h3>ID</h3>
      <Input value={dataRead.id}></Input>
      <h3>Mô tả</h3>
      <Input value={dataRead.description}></Input>
      <h3>Dự án số</h3>
      <Input value={dataRead.projectId}></Input>
      <h3>Diện tích</h3>
      <Input value={dataRead.acreage}></Input>
      <h3>Danh sách căn hộ</h3>
      <Input value={dataRead.apartmentList}></Input>
      <h3>Hình ảnh</h3>
      <Input value={dataRead.photo}></Input>
      <h3>Ngày tạo</h3>
      <Input value={dataRead.dateCreate}></Input>
      <h3>Ngày cập nhật</h3>
      <Input value={dataRead.dateUpdate}></Input>
    </Modal>
  );
}
