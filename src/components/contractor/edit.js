import React, { useEffect, useState } from "react";
import Modal from "antd/lib/modal/Modal";
import { Input } from "antd";
import "antd/dist/antd.css";

export default function Edit({ trangThai, callbackToggle, dataEdit }) {
  const [data, setData] = useState({
    name: "",
    description: "",
    projects: "",
    address:"",
    phone:"",
    phone2:"",
    fax:"",
    email:"",
    website:"",
    note:"",
  });
  return (
    <Modal
      title="Chỉnh sửa thông tin nhà thầu xây dựng"
      centered
      visible={trangThai}
      onOk={() => {
        console.log(data);
        if (!data.name || !data.description || !data.projects || !data.address || !data.phone || !data.phone2 || !data.fax || !data.email || !data.website || !data.note) {
          alert("Cần nhập đầy đủ dữ liệu");
        } else {
          let response = fetch(
            "http://localhost:8090/contractor/" + dataEdit.id,
            {
              method: "PUT",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(data),
            }
          )
            .then((data) => data.json())
            .then((data) => {
              alert("Cập nhật nhà thầu xây dựng thành công")
              callbackToggle(false);
            });
        }
      }}
      onCancel={() => callbackToggle(false)}
      width={1000}
    > 
      <h3>ID</h3>
      <Input
        disabled
        placeholder={dataEdit.id}
      ></Input>
      <h3>Tên</h3>
      <Input
        placeholder={dataEdit.name}
        onChange={(event) => (data.name = event.target.value)}
      ></Input>
      <h3>Mô tả</h3>
      <Input
        placeholder={dataEdit.description}
        onChange={(event) => (data.description = event.target.value)}
      ></Input>
      <h3>Dự án</h3>
      <Input
        placeholder={dataEdit.projects}
        onChange={(event) => (data.projects = event.target.value)}
      ></Input>
      <h3>Địa chỉ</h3>
      <Input
        placeholder={dataEdit.address}
        onChange={(event) => (data.address = event.target.value)}
      ></Input>
      <h3>Số điện thoại</h3>
      <Input
        placeholder={dataEdit.phone}
        onChange={(event) => (data.phone = event.target.value)}
      ></Input>
      <h3>Số điện thoại khác</h3>
      <Input
        placeholder={dataEdit.phone2}
        onChange={(event) => (data.phone2 = event.target.value)}
      ></Input>
      <h3>Fax</h3>
      <Input
        placeholder={dataEdit.fax}
        onChange={(event) => (data.fax = event.target.value)}
      ></Input>
      <h3>Email</h3>
      <Input
        placeholder={dataEdit.email}
        onChange={(event) => (data.email = event.target.value)}
      ></Input>
      <h3>Trang chủ</h3>
      <Input
        placeholder={dataEdit.website}
        onChange={(event) => (data.website = event.target.value)}
      ></Input>
      <h3>Ghi chú</h3>
      <Input
        placeholder={dataEdit.note}
        onChange={(event) => (data.note = event.target.value)}
      ></Input>
      
    </Modal>
  );
}
